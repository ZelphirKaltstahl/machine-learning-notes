;; ==========================
;; ORG-MODE SPECIFIC SETTINGS
;; ==========================
(require 'org)
(require 'ox-latex)
;; Setup for exporting to PDF using Xelatex First you need to define a
;; document class, which will be referred to later, that has all the
;; definitions required to compile the PDF.

;; EXAMPLE
(with-eval-after-load 'ox-latex
  (add-to-list 'org-latex-classes
               '("org-book"
                 "
\\documentclass[12pt, a4paper, openany, twoside]{book}
\\usepackage[vmargin=2cm,
             rmargin=2cm,
             lmargin=2cm,
             bindingoffset=1cm]{geometry}
\\usepackage{mathtools}
\\usepackage[UTF8]{ctex}
\\usepackage[autostyle=true]{csquotes}  % modify quotes
\\usepackage[ngerman,USenglish]{babel}  % the last language in this line will be default for the document


 %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% TABLE OF CONTENTS %%%%%
 %%%%%%%%%%%%%%%%%%%%%%%%%%%
% styling for article document class
% \\usepackage{tocloft}
\\usepackage[rightlabels, dotinlabels]{titletoc}
\\setcounter{chapter}{1}

% level of depth of table of contents and section numberings
\\setcounter{tocdepth}{2}
\\setcounter{secnumdepth}{2}


 %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% LANGUAGE SETTINGS %%%%%
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (important for automatic breaking words)
% \\selectlanguage{ngerman}
\\selectlanguage{USenglish}


 %%%%%%%%%%%%%%%%%%%%
%%%%% PAGE STYLE %%%%%
 %%%%%%%%%%%%%%%%%%%%
\\raggedbottom
\\pagestyle{empty}


\\title{}
     [NO-DEFAULT-PACKAGES]
      [NO-PACKAGES]

 %%%%%%%%%%%%%%%%%%%
%%%%% MATH MODE %%%%%
 %%%%%%%%%%%%%%%%%%%
\\newcommand{\\Hsquare}{%
    \\text{\\fboxsep=-.15pt\\fbox{\\rule{0pt}{.75ex}\\rule{.75ex}{0pt}}}%
}

\\newcommand{\\divides}{\\mid}
\\newcommand{\\notdivides}{\\nmid}
\\newcommand{\\setR}{\\mathbb{R}}
\\newcommand{\\setQ}{\\mathbb{Q}}
\\newcommand{\\setZ}{\\mathbb{Z}}
\\newcommand{\\setN}{\\mathbb{N}}

\\usepackage{etoolbox}% for '\\AtBeginEnvironment' macro
\\AtBeginEnvironment{pmatrix}{\\everymath{\\displaystyle}}
\\AtBeginEnvironment{bmatrix}{\\everymath{\\displaystyle}}
\\AtBeginEnvironment{matrix}{\\everymath{\\displaystyle}}
\\AtBeginEnvironment{array}{\\everymath{\\displaystyle}}

% absolute value
\\DeclarePairedDelimiter\\abs{\\lvert}{\\rvert}
\\DeclarePairedDelimiter\\norm{\\lVert}{\\rVert}

% Swap the definition of \\abs* and \\norm*, so that \\abs
% and \\norm resizes the size of the brackets, and the
% starred version does not.
\\makeatletter
\\let\\oldabs\\abs
\\def\\abs{\\@ifstar{\\oldabs}{\\oldabs*}}
%
\\let\\oldnorm\\norm
\\def\\norm{\\@ifstar{\\oldnorm}{\\oldnorm*}}
\\makeatother


 %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% PREDEFINED COLORS %%%%%
 %%%%%%%%%%%%%%%%%%%%%%%%%%%
\\usepackage{color}
\\usepackage[table]{xcolor}
\\definecolor{quotecolor}{HTML}{686868}
\\definecolor{White}{RGB}{255, 255, 255}
\\definecolor{Black}{RGB}{0, 0, 0}
\\definecolor{tableHeader}{RGB}{211, 47, 47}
\\definecolor{tableLineOdd}{RGB}{245, 245, 245}
\\definecolor{tableLineEven}{RGB}{224, 224, 224}
\\definecolor{linkgray}{RGB}{100, 100, 100}
\\definecolor{CadetBlue}{RGB}{110, 106, 156}

 %%%%%%%%%%%%%%%
%%%%% FONTS %%%%%
 %%%%%%%%%%%%%%%
\\setCJKmainfont[Scale=1.0]{WenQuanYi Micro Hei}
\\setmainfont[Scale=1]{Ubuntu}
\\setmonofont{Liberation Mono}
\\DeclareMathSizes{14}{12}{10}{8}
"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                 ("\\paragraph{%s}" . "\\paragraph*{%s}")
                 ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))

(setq org-latex-pdf-process
      '("xelatex -interaction nonstopmode %f"
        "xelatex -interaction nonstopmode %f")) ;; for multiple passes
